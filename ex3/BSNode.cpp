#include "BSNode.h"

template <class data>
BSNode<data>::BSNode(data data)
{
	_data = data;
	_count = 1;
	_left = nullptr;
	_right = nullptr;
}

template <class data>
BSNode<data>::BSNode(const BSNode& other)
{
	_data = other.getData();
	if (other._left)
	{
		insert(other._left->getData());
	}
	else
	{
		_left = nullptr;
	}
	if (other._right)
	{
		insert(other._right->getData());
	}
	else
	{
		_right = nullptr;
	}
	_count = other._count;
}

template <class data>
BSNode<data>::~BSNode()
{
	if (_left)
	{
		delete _left;
	}
	if (_right)
	{
		delete _right;
	}
}

template <class data>
void BSNode<data>::insert(data value)
{
	if (value > _data)
	{
		if (_right)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
			_right->_left = nullptr;
			_right->_right = nullptr;
		}
	}
	else if (value < _data)
	{
		if (_left)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
			_left->_left = nullptr;
			_left->_right = nullptr;
		}
	}
	else
	{
		_count++;
	}
}

template <class data>
void BSNode<data>::printNodes() const
{
	if (_left)
	{
		_left->printNodes();
	}
	std::cout << _data << " - " << _count << "\n";
	if (_right)
	{
		_right->printNodes();
	}
}

template <class data>
BSNode<data> BSNode<data>::operator=(const BSNode& other)
{
	return new BSNode(other);
}

template <class data>
bool BSNode<data>::isLeaf() const
{
	if (_left || _right)
	{
		return false;
	}
	else
	{
		return true;
	}
}

template <class data>
data BSNode<data>::getData() const
{
	return _data;
}

template <class data>
BSNode<data>* BSNode<data>::getLeft() const
{
	return _left;
}

template <class data>
BSNode<data>* BSNode<data>::getRight() const
{
	return _right;
}

template <class data>
bool BSNode<data>::search(data val) const
{
	if (val == _data)
	{
		return true;
	}
	else if (isLeaf())
	{
		return false;
	}

	if ((_left && _left->search(val)) || (_right && _right->search(val)))
	{
		return true;
	}
	else
	{
		return false;
	}
}

template <class data>
int BSNode<data>::getHeight() const
{
	int left, right;
	if (!isLeaf())
	{
		if ((_left && _right && _left->getHeight() > _right->getHeight()) || (_left && !_right))
		{
			return _left->getHeight() + 1;
		}
		else
		{
			return _right->getHeight() + 1;
		}
	}
	else
	{
		return 0;
	}
}

template <class data>
int BSNode<data>::getDepth(const BSNode& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}

template <class data>
int BSNode<data>::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	int left, right;
	if (search(node->getData()) == 0)
	{
		return -1;
	}
	if (node->getData() == this->getData())
	{
		return 0;
	}
	else
	{
		_left ? left = _left->getCurrNodeDistFromInputNode(node) : left = 0;
		_right ? right = _right->getCurrNodeDistFromInputNode(node) : right = 0;
		if ((_left && _right && left > right) || (_left && !_right))
		{
			return left + 1;
		}
		else
		{
			return right + 1;
		}
	}
}

