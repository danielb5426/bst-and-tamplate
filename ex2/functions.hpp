#pragma once
#include <iostream>

template <typename T>
void bubbleSort(T arr[], const int size)
{
	int i, j;
	T temp;
	for (i = 0; i < size; i++)
	{
		for (j = i + 1; j < size; j++)
		{
			if (arr[j] < arr[i])
			{
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

template <typename T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}

template <typename T>
int compare(T object1, T object2)
{
	if (object1 == object2)
	{
		return 0;
	}
	else if (object1 < object2)
	{
		return 1;
	}
	return -1;
}
