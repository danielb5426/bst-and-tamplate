#pragma once
#include <iostream>

template <class data>
class BSNode
{
public:

	BSNode(data data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(data value);
	BSNode operator=(const BSNode& other);

	bool isLeaf() const;
	data getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(data val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	data _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};

