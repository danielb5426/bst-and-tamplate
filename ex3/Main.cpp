#include <iostream>
#include "BSNode.cpp"


int main()
{

	std::string stringArr[] = { "w", "e", "b", "c", "g", "f", "d", "s", "p", "o", "i", "u", "y", "t", "a" };
	int intArr[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 12, 23, 26, 67, 10, 99 };

	for (int i = 0; i < 15; i++)
	{
		std::cout << intArr[i] << ' ';
	}

	for (int i = 0; i < 15; i++)
	{
		std::cout << stringArr[i] << '\n';
	}
	
	BSNode<int> intArray(intArr[0]);

	for (int i = 1; i < 15; i++)
	{
		intArray.insert(intArr[i]);
	}

	BSNode<std::string> stringArray(stringArr[0]);

	for (int i = 1; i < 15; i++)
	{
		stringArray.insert(stringArr[i]);
	}

	intArray.printNodes();
	stringArray.printNodes();

	system("pause");
	return 0;
}