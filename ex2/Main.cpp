
#include <iostream>
#include "functions.hpp"

class Num
{
public:

	int data;

	Num(int num=0)
	{
		data = num;
	}

	friend std::ostream& operator<<(std::ostream& out, const Num v)
	{
		return out << v.data;
	}

	bool operator==(const Num other) const
	{
		return data == other.data;
	}

	bool operator<(const Num other) const
	{
		return data < other.data;
	}

	Num& operator=(const Num other)
	{
		data = other.data;
		return *this;
	}
};

int main() {

	//CHAR
	//check compare
	std::cout << "char check:\ncorrect print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'z') << std::endl;
	std::cout << compare<char>('z', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;
	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;
	const int arr_size = 5;
	char charArr[arr_size] = { 'b', 'f', 'a', 'd', 'c' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;
	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;

	//FLOAT
	//check compare
	std::cout << "float check:\ncorrect print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;
	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;

	//CLASS
	//check compare
	Num num1(3), num2(5);
	std::cout << "class check:\ncorrect print is 1 -1 0" << std::endl;
	std::cout << compare<Num>(num1, num2) << std::endl;
	std::cout << compare<Num>(num2, num1) << std::endl;
	std::cout << compare<Num>(num1, num1) << std::endl;
	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;
	Num numArr[arr_size] = { *(new Num(5)), *(new Num(4)), *(new Num(3)), *(new Num(2)), *(new Num(1)) };
	bubbleSort<Num>(numArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << numArr[i] << " ";
	}
	std::cout << std::endl;
	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<Num>(numArr, arr_size);

	system("pause");
	return 1;
}












