#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

using std::cout;
using std::endl;

int main()
{
	BSNode* printCheck(new BSNode("z"));
	printCheck->insert("q");
	printCheck->insert("w");
	printCheck->insert("e");
	printCheck->insert("r");
	printCheck->insert("t");
	printCheck->insert("y");
	printCheck->insert("u");
	printCheck->insert("i");
	printCheck->insert("o");
	printCheck->insert("p");

	printCheck->insert("z");
	printCheck->insert("q");
	printCheck->insert("w");
	printCheck->insert("e");
	printCheck->insert("r");
	printCheck->insert("t");
	printCheck->insert("y");
	printCheck->insert("u");
	printCheck->insert("i");
	printCheck->insert("o");
	printCheck->insert("p");

	printCheck->printNodes();

	BSNode* bs(new BSNode("6"));
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("7");

	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "Tree height: " << bs->getLeft()->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;


	std::string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;

	return 0;
}
