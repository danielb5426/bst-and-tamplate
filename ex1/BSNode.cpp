#include "BSNode.h"

BSNode::BSNode(std::string data)
{
	_data = data;
	_count = 1;
	_left = nullptr;
	_right = nullptr;
}

BSNode::BSNode(const BSNode& other)
{
	_data = other.getData();
	if (other._left)
	{
		insert(other._left->getData());
	}
	else
	{
		_left = nullptr;
	}
	if (other._right)
	{
		insert(other._right->getData());
	}
	else
	{
		_right = nullptr;
	}
	_count = other._count;
}

BSNode::~BSNode()
{
	if (_left)
	{
		delete _left;
	}
	if (_right)
	{
		delete _right;
	}
}

void BSNode::insert(std::string value)
{
	if (value > _data)
	{
		if (_right)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
			_right->_left = nullptr;
			_right->_right = nullptr;
		}
	}
	else if (value < _data)
	{
		if (_left)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
			_left->_left = nullptr;
			_left->_right = nullptr;
		}
	}
	else
	{
		_count++;
	}
}

void BSNode::printNodes() const
{
	if (_left)
	{
		_left->printNodes();
	}
	std::cout << _data << " - " << _count << "\n";
	if (_right)
	{
		_right->printNodes();
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	return *(new BSNode(other));
}

bool BSNode::isLeaf() const
{
	if(_left || _right)
	{
		return false;
	}
	else
	{
		return true;
	}
}

std::string BSNode::getData() const
{
	return _data;
}

BSNode* BSNode::getLeft() const
{
	return _left;
}

BSNode* BSNode::getRight() const
{
	return _right;
}

bool BSNode::search(std::string val) const
{
	if (val == _data)
	{
		return true;
	}
	else if (isLeaf())
	{
		return false;
	}

	if ((_left && _left->search(val)) || (_right && _right->search(val)))
	{
		return true;
	}
	else
	{
		return false;
	}
}

int BSNode::getHeight() const
{
	int left, right;
	if (!isLeaf())
	{
		if ((_left && _right && _left->getHeight() > _right->getHeight()) || (_left && !_right))
		{
			return _left->getHeight() + 1;
		}
		else
		{
			return _right->getHeight() + 1;
		}
	}
	else
	{
		return 0;
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	int left, right;
	if (search(node->getData()) == 0)
	{
		return -1;
	}
	if (node->getData() == this->getData())
	{
		return 0;
	}
	else
	{
		_left ? left = _left->getCurrNodeDistFromInputNode(node) : left = 0;
		_right ? right = _right->getCurrNodeDistFromInputNode(node) : right = 0;
		if ((_left && _right && left > right) || (_left && !_right))
		{
			return left + 1;
		}
		else
		{
			return right + 1;
		}
	}
}











